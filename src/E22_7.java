////////////////////////////////////////
//  Author: Darwish Quiambao
//  Course Number: CS49J
//  Date Created: 11/30/2020
//  Description: This program counts words in each file provided in the command line argument
//  it demonstrates the use of multithreading in Java.
///////////////////////////////////////
import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class WordCount implements Runnable {
    //For handling files
    private File f;
    //Counting words
    private int nWordCounter;
    private Lock wordCountLock;
    private static int wordsTotal;
    private String fName;
    private boolean fileExists;

    public WordCount(File f) {
        if (f.exists()) {
            this.f = new File(f.getAbsolutePath());
            fileExists = true;
        } else {
            fName = f.getName();
            fileExists = false;
            System.out.println("File not found! " + "("+f.getName() +")");
        }
        wordCountLock = new ReentrantLock();
        nWordCounter = 0;
    }
    public synchronized void setWordsTotal()
    {
        wordsTotal += nWordCounter;
    }
    @Override
    public void run() {
        wordCountLock.lock();
        try{
            if(!fileExists)
            {
                System.out.println(fName + ": " + nWordCounter);
            }
            else {
                //For reading the file
                String line;
                File file = new File(f.getAbsolutePath());
                FileInputStream fstream = new FileInputStream(file);
                InputStreamReader in = new InputStreamReader(fstream);
                BufferedReader bufferedReader = new BufferedReader(in);
                //Read file and separate the words into a string array
                //Use the length of this string array as the number of words.
                while ((line = bufferedReader.readLine()) != null) {
                    String[] words = line.trim().split("\\s+");
                    nWordCounter = words.length;
                }
                setWordsTotal();
                System.out.println(f.getName() + ": " + nWordCounter + " words." + " ");
                fstream.close();
            }
        }
        catch (NullPointerException | IOException n)
        {
            System.out.println(f.getName() + ": " + nWordCounter);
        } finally {
            wordCountLock.unlock();
        }
    }
    public static int getWordsTotal()
    {
        return wordsTotal;
    }
}

public class E22_7 {
    public static void main(String[] args) {

        //For loading up the multiple files.
        ArrayList<File> files = new ArrayList<>();
        if (args.length > 0) {
            for (String arg : args) {
                files.add(new File(arg));
            }

        }
        else
        {
            //Provide Usage
            System.out.println("No filename passed...");
            System.out.println("Usage: java E22_7 [arg1] [arg2] etc. (Ex: java E22_7 file1.txt file2.txt)\n" + "Rerun program again using CLI"
            + " following the above example usage.");
            System.exit(-1);
        }

        try {
            for (File a : files) {
                Runnable r = new WordCount(a);
                Thread t = new Thread(r);
                t.start();
                t.join();
            }
        }
        catch(Exception e)
        {
            e.getStackTrace();
        }
        System.out.println("Combined words: " + WordCount.getWordsTotal() + " words.");
    }
}