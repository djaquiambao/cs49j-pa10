////////////////////////////////////////
//  Author: Darwish Quiambao
//  Course Number: CS49J
//  Date Created: 11/28/2020
//  Description: This program demonstrates use of Generic Classes by finding the maximum and minimum numbers
//  in a given set.
///////////////////////////////////////
interface Measurable
{
    /**
     Computes the measure of the object.
     @return the measure
     */
    double getMeasure();
}

class Numbers implements Measurable
{
    private double dValue;

    public Numbers (double val)
    {
        dValue = val;
    }

    @Override
    public double getMeasure() {
        return dValue;
    }

    @Override
    public String toString() {
        return "" + dValue + "";
    }
}

class Pair<T, S>
{
    private T first;
    private S second;

    public Pair(T firstElement, S secondElement)
    {
        first = firstElement;
        second = secondElement;
    }

    public T getFirst()
    {
        return first;
    }

    public S getSecond()
    {
        return second;
    }
}

class PairUtil
{
    public static <T extends Measurable> Pair<T,T> minmax(T[] objects)
    {
        T smallest = objects[0];
        T largest = objects[1];
        for(int i = 1; i < objects.length; i++)
        {
            if(objects[i].getMeasure() < smallest.getMeasure())
            {
                smallest = objects[i];
            }
            else
            {
                largest = objects[i];
            }
        }
        return new Pair<>(smallest, largest);
    }

}

public class P18_1
{
    public static void main(String[] args)
    {
        Measurable[] numbers = {new Numbers(3), new Numbers(5.5), new Numbers(99.0), new Numbers(1)};
        Pair<Measurable, Measurable> minmaxPairs= PairUtil.minmax(numbers);
        System.out.println("("+ minmaxPairs.getFirst() + ", " + minmaxPairs.getSecond() + ")");
    }
}
