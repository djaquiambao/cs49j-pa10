////////////////////////////////////////
//  Author: Darwish Quiambao
//  Course Number: CS49J
//  Date Created: 11/29/2020
//  Description: This program extends the class Measurable, in this program named MeasurableGeneric<T>
//
///////////////////////////////////////
import java.util.ArrayList;

class MeasurableGeneric<T>
{
    private T value;

    public MeasurableGeneric(T val)
    {
        value = val;
    }

    T getMeasure()
    {
        return value;
    }

    //Extends Comparable so we can combare the objects
    public static <T extends Comparable<T>> T largest(ArrayList<T> objects)
    {
        T largest = objects.get(0);
        for(int i = 1; i < objects.size(); i++)
        {
            if(largest.compareTo(objects.get(i)) < 0)
            {
                largest = objects.get(i);
            }
        }
        return largest;
    }

}

public class P18_4 {
    public static void main(String[] args) {
        ArrayList<Integer> measurableNumbers = new ArrayList<>();
        measurableNumbers.add(3);
        measurableNumbers.add(8);
        measurableNumbers.add(10);
        measurableNumbers.add(6);
        measurableNumbers.add(9);
        System.out.println("Input: " + measurableNumbers);
        System.out.println("Largest: " + MeasurableGeneric.largest(measurableNumbers));

    }
}
